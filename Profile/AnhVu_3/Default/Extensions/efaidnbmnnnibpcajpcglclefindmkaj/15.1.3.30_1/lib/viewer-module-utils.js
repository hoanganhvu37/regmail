/*************************************************************************
* ADOBE CONFIDENTIAL
* ___________________
*
*  Copyright 2015 Adobe Systems Incorporated
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of Adobe Systems Incorporated and its suppliers,
* if any.  The intellectual and technical concepts contained
* herein are proprietary to Adobe Systems Incorporated and its
* suppliers and are protected by all applicable intellectual property laws,
* including trade secret and or copyright laws.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from Adobe Systems Incorporated.
**************************************************************************/
function dependOn(){"use strict";return[require("common"),require("util"),require("floodgate")]}var def;require=function(e){"use strict";return e},def=window.define?window.define:function(e,o){"use strict";return o.apply(null,[{ajax:$.ajax.bind($)}])};var exports=acom_analytics={};def(dependOn(),(function(e,o,t){var i;for(prop in i||(i=new function(){this.updateVariables=async function(t){try{let i=0!=t&&1!=t,r=!(!i||t===SETTINGS.READER_VER||t===SETTINGS.ERP_READER_VER);o.setCookie("locale",o.getFrictionlessLocale(chrome.i18n.getMessage("@@ui_locale"))),o.setCookie("cdnUrl",e.getAcrobatViewerUri()),o.setCookie("isDeskTop",i),o.setCookie("env",e.getEnv()),o.setCookie("viewerImsClientId",e.getViewerIMSClientId()),o.setCookie("viewerImsClientIdSocial",e.getViewerIMSClientIdSocial()),o.setCookie("imsURL",e.getIMSurl()),o.setCookie("imsLibUrl",e.getImsLibUrl()),o.setCookie("dcApiUri",e.getDcApiUri()),o.setCookie("isAcrobat",r),this.checkDomainRollbackFlag({flag:"dc-cv-use-old-domain"});let a=[this.checkFeatureEnable({flag:"dc-cv-modern-viewer",storageKey:"modernViewerEnable"}),this.checkFeatureEnable({flag:"dc-cv-dark-mode",storageKey:"darkModeEnable"}),this.checkFeatureEnable({flag:"dc-cv-read-aloud",storageKey:"isReadAloudEnable"})];return window.navigator.onLine&&a.push(this.checkFeatureEnable({flag:"dc-cv-offline-support-disable",storageKey:"offlineSupportDisable"})),Promise.all(a).then(e=>{const t=e[1];o.getCookie("theme")&&!t?o.removeCookie("theme"):t&&!o.getCookie("theme")&&o.setCookie("theme","auto")})}catch(e){}},this.checkFeatureEnable=function(e){return new Promise(i=>{t.hasFlag(e.flag).then(t=>{e.storageKey&&o.setCookie(e.storageKey,!!t),i(t)})})},this.checkDomainRollbackFlag=function(o){t.hasFlag(o.flag).then(o=>{e.setOldDomainRollback(!!o)})}}),i)i.hasOwnProperty(prop)&&("function"==typeof i[prop]?exports[prop]=i[prop].bind(i):exports[prop]=i[prop]);return i}));