/*************************************************************************
* ADOBE CONFIDENTIAL
* ___________________
*
*  Copyright 2015 Adobe Systems Incorporated
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of Adobe Systems Incorporated and its suppliers,
* if any.  The intellectual and technical concepts contained
* herein are proprietary to Adobe Systems Incorporated and its
* suppliers and are protected by all applicable intellectual property laws,
* including trade secret and or copyright laws.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from Adobe Systems Incorporated.
**************************************************************************/
!function(){let e;function i(e){const i={main_op:"analytics"};i.analytics=[[e]],chrome.runtime.sendMessage(i)}function n(){const n=util.getTabCookie("csrf"),o=signInUtil.parseCSRF(new URL(e));util.removeTabCookie("csrf");!n||!o||o!==n?(i("DCBrowserExt:Viewer:User:Error:NonMatchingCsrfToken:FailedToLogin"),signInUtil.sign_out(e)):function(){try{let n=e;if(n&&n.indexOf("#")>-1)if(n.indexOf("signIn=true")>-1){i("DCBrowserExt:Viewer:Ims:Sign:In:"+(util.getTabCookie("signInSource")?util.getTabCookie("signInSource"):"Unknown")+":Successful"),i("DCBrowserExt:Viewer:Ims:Sign:In:Successful"),util.removeTabCookie("signInSource")}else if(n.indexOf("signUp=true")>-1){i("DCBrowserExt:Viewer:Ims:Sign:Up:"+(util.getTabCookie("signUpSource")?util.getTabCookie("signUpSource"):"Unknown")+":Successful"),i("DCBrowserExt:Viewer:Ims:Sign:Up:Successful"),util.removeTabCookie("signUpSource")}let o=signInUtil.getSearchParamFromURL("mimePdfUrl",e),t=o&&decodeURIComponent(o);chrome.tabs.update({url:t})}catch(e){}}()}const o=()=>{chrome.tabs.query({active:!0,lastFocusedWindow:!0},(function(i){if(i[0]){e=i[0].url;if(!(e.indexOf("access_token")>-1&&util.getTabCookie("signInSource"))){let i=signInUtil.getSearchParamFromURL("mimePdfUrl",e),n=i&&decodeURIComponent(i);return void chrome.tabs.update({url:n})}n()}}))};chrome.tabs.query({active:!0,lastFocusedWindow:!0},(function(i){if(i[0])if(e=i[0].url,new URLSearchParams(new URL(e).search).get("socialSignIn")){const i=util.getTabCookie("idp_token");let n=function(e){let i=new URL(window.location.href),n=new URLSearchParams(new URL(window.location.href).search);return n.delete(e),i.search=n,i}("socialSignIn");e=n.href,signInUtil.socialSignIn(i,n),util.removeTabCookie("idp_token")}else o()}))}();