import random


class genName(object):
    def __init__(self, type):
        self.type = type

    def create(self):
        data = {'ho' : '', 'ten' : ''}
        f_ho = open("list_ho.txt", "r", encoding="utf-8")
        list_ho = []
        for item in f_ho:
            list_ho.append(item)
        data['ho'] = random.choice(list_ho)[0:-1]

        if self.type == 1:
            f_ten = open("ten_nam.txt", "r", encoding="utf-8")
            list_ten = []
            for item in f_ten:
                list_ten.append(item)
            data['ten'] = random.choice(list_ten)[0:-1]
            return data
        else:
            f_ten = open("ten_nu.txt", "r", encoding="utf-8")
            list_ten = []
            for item in f_ten:
                list_ten.append(item)
            data['ten'] = random.choice(list_ten)[0:-1]
            return data

    def createEmail(self, full_name):
        s = ''
        s1 = u'ÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚÝàáâãèéêìíòóôõùúýĂăĐđĨĩŨũƠơƯưẠạẢảẤấẦầẨẩẪẫẬậẮắẰằẲẳẴẵẶặẸẹẺẻẼẽẾếỀềỂểỄễỆệỈỉỊịỌọỎỏỐốỒồỔổỖỗỘộỚớỜờỞởỠỡỢợỤụỦủỨứỪừỬửỮữỰựỲỳỴỵỶỷỸỹ'
        s0 = u'AAAAEEEIIOOOOUUYaaaaeeeiioooouuyAaDdIiUuOoUuAaAaAaAaAaAaAaAaAaAaAaAaEeEeEeEeEeEeEeEeIiIiOoOoOoOoOoOoOoOoOoOoOoOoUuUuUuUuUuUuUuYyYyYyYy'
        for c in full_name:
            if c in s1:
                s += s0[s1.index(c)]
            else:
                s += c
        return s

    def createPassword(self, number):
        s = ''
        s1 = ['QWERYUIOPASDFGHJKLZXCVBNM', 'qwertyuiopasdfghjklmnbvcz', '0123456789', '!@#$%^&*()']
        for i in range(0, number):
            if i > (number - number / 3):
                s += s1[3][random.randint(0, len(s1[3])) - 1]
            elif i > (number - number / 2):
                s += s1[2][random.randint(0, len(s1[2])) - 1]
            else:
                x = random.randint(0, 1)
                s += s1[x][random.randint(0, len(s1[x])) - 1]
        return s
