from time import sleep
import undetected_chromedriver as uc
import os
import threading
class myThread(threading.Thread):
    def __init__(self, name):
        threading.Thread.__init__(self)
        self.name = name
    def run(self):
        print("Đã sẵn sàn chạy thread " + self.name)
        options = uc.ChromeOptions()
        pathProfile = os.path.join(os.getcwd(), 'Profile', self.name)
        options.add_argument('--user-data-dir=' + pathProfile)
        chrome = uc.Chrome(options=options)

        chrome.get('https://google.com')
        sleep(1000000)
if __name__ == '__main__':
    thread_1 = myThread("AnhVu_3")
    thread_2 = myThread("AnhVu_4")
    thread_1.start()
    thread_2.start()
